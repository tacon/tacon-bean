package dev.tacon.bean;


public class BeanProperty<A, P, B, T> {

	public static class RootBeanProperty<B, T> extends BeanProperty<B, Void, B, T> {

		public RootBeanProperty(final Class<B> beanType, final String name, final Class<T> type) {
			super(null, beanType, name, type);
		}
	}

	final BeanProperty<A, ?, P, B> parent;
	final Class<? super B> beanType;
	final String name;
	final Class<T> type;

	protected BeanProperty(final BeanProperty<A, ?, P, B> parent, final Class<? super B> beanType, final String name, final Class<T> type) {
		this.parent = parent;
		this.beanType = beanType;
		this.name = name;
		this.type = type;
	}

	public Class<? super B> getBeanType() {
		return this.beanType;
	}

	public String getName() {
		return this.name;
	}

	public Class<T> getType() {
		return this.type;
	}

	public BeanProperty<A, ?, P, B> getParent() {
		return this.parent;
	}

	public <X> BeanProperty<A, B, ? super T, X> then(final BeanProperty<?, ?, ? super T, X> property) {
		return new BeanProperty<>(this, property.beanType, property.name, property.type);
	}

	public String getNestedName() {
		if (this.parent == null) {
			return this.name;
		}
		return this.parent.getNestedName() + "." + this.name;
	}

	@Override
	public String toString() {
		return this.getNestedName();
	}
}
